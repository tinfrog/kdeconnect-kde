# Translation of kdeconnect-indicator.po to Brazilian Portuguese
# Copyright (C) 2020 This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2020.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2021, 2023.
# Geraldo Simiao <geraldosimiao@fedoraproject.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-07 00:37+0000\n"
"PO-Revision-Date: 2023-08-11 00:52-0300\n"
"Last-Translator: Geraldo Simiao <geraldosimiao@fedoraproject.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "André Marcelo Alvarenga"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alvarenga@kde.org"

#: deviceindicator.cpp:53
#, kde-format
msgid "Browse device"
msgstr "Navegar no dispositivo"

#: deviceindicator.cpp:67
#, kde-format
msgid "Send clipboard"
msgstr "Enviar área de transferência"

#: deviceindicator.cpp:81
#, kde-format
msgctxt "@action:inmenu play bell sound"
msgid "Ring device"
msgstr "Fazer o dispositivo tocar"

#: deviceindicator.cpp:97
#, kde-format
msgid "Send a file/URL"
msgstr "Enviar um arquivo/URL"

#: deviceindicator.cpp:107
#, kde-format
msgid "SMS Messages..."
msgstr "Mensagens SMS..."

#: deviceindicator.cpp:120
#, kde-format
msgid "Run command"
msgstr "Executar comando"

#: deviceindicator.cpp:122
#, kde-format
msgid "Add commands"
msgstr "Adicionar comandos"

#: indicatorhelper_mac.cpp:37
#, kde-format
msgid "Launching"
msgstr "Iniciando"

#: indicatorhelper_mac.cpp:87
#, kde-format
msgid "Launching daemon"
msgstr "Iniciando serviço"

#: indicatorhelper_mac.cpp:96
#, kde-format
msgid "Waiting D-Bus"
msgstr "Aguardando D-Bus"

#: indicatorhelper_mac.cpp:113 indicatorhelper_mac.cpp:125
#: indicatorhelper_mac.cpp:142
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: indicatorhelper_mac.cpp:114 indicatorhelper_mac.cpp:126
#, kde-format
msgid ""
"Cannot connect to DBus\n"
"KDE Connect will quit"
msgstr ""
"Não foi possível conectar ao D-Bus\n"
"O KDE Connect será fechado"

#: indicatorhelper_mac.cpp:142
#, kde-format
msgid "Cannot find kdeconnectd"
msgstr "Não foi possível encontrar o kdeconnectd"

#: indicatorhelper_mac.cpp:147
#, kde-format
msgid "Loading modules"
msgstr "Carregando módulos"

#: main.cpp:53
#, kde-format
msgid "KDE Connect Indicator"
msgstr "Indicador do KDE Connect"

#: main.cpp:55
#, kde-format
msgid "KDE Connect Indicator tool"
msgstr "Ferramenta Indicador do KDE Connect"

#: main.cpp:57
#, kde-format
msgid "(C) 2016 Aleix Pol Gonzalez"
msgstr "(C) 2016 Aleix Pol Gonzalez"

#: main.cpp:92
#, kde-format
msgid "Configure..."
msgstr "Configurar..."

#: main.cpp:114
#, kde-format
msgid "Pairing requests"
msgstr "Pedidos de emparelhamento"

#: main.cpp:119
#, kde-format
msgctxt "Accept a pairing request"
msgid "Pair"
msgstr "Emparelhar"

#: main.cpp:120
#, kde-format
msgid "Reject"
msgstr "Rejeitar"

#: main.cpp:126 main.cpp:136
#, kde-format
msgid "Quit"
msgstr "Sair"

#: main.cpp:155 main.cpp:179
#, kde-format
msgid "%1 device connected"
msgid_plural "%1 devices connected"
msgstr[0] "%1 dispositivo conectado"
msgstr[1] "%1 dispositivos conectados"

#: systray_actions/battery_action.cpp:28
#, kde-format
msgid "No Battery"
msgstr "Sem bateria"

#: systray_actions/battery_action.cpp:30
#, kde-format
msgid "Battery: %1% (Charging)"
msgstr "Bateria: %1% (Carregando)"

#: systray_actions/battery_action.cpp:32
#, kde-format
msgid "Battery: %1%"
msgstr "Bateria: %1%"

#: systray_actions/connectivity_action.cpp:30
#, kde-format
msgctxt ""
"The fallback text to display in case the remote device does not have a "
"cellular connection"
msgid "No Cellular Connectivity"
msgstr "Sem conectividade celular"

#: systray_actions/connectivity_action.cpp:33
#, kde-format
msgctxt ""
"Display the cellular connection type and an approximate percentage of signal "
"strength"
msgid "%1  | ~%2%"
msgstr "%1  | ~%2%"

#~ msgid "Get a photo"
#~ msgstr "Tirar uma foto"

#~ msgid "Select file to send to '%1'"
#~ msgstr "Selecione o arquivo a enviar para '%1'"
